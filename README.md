# Postlight Code Challenge

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). It depicts a faux employee directory with a search feature on the person's first/last name, a "copy to clipboard" feature for employee information, and an admin route to perform basic CRUD operations on existing and new employees.

## Getting Started

In the project directory after running `npm install`, you can:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

## Notes

This project makes use of session storage rather than an API for all CRUD operations as to focus on other features.

Other features to add include testing (beyond the built-in create-react-app tests), cross-browser compatibility checks, pagination, input field validation, and file upload capabilities for photos (this project dynamically generates photos using the PlaceKitten API). Filter values could also be expanded to include features for creating/updating/deleting new departments, locations, etc.

## Resources
[Google Fonts](https://fonts.google.com)
[FontAwesome](https://fontawesome.com)
[RandomUser Generator](https://randomuser.me/)
[PlaceKitten API](http://placekitten.com/)
[Spinner SVG](https://loading.io/spinner/ripple/-ripple-radio-preloader)
