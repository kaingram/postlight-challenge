
const searchFilters = [
    {
        category: 'Department',
        values: ['Engineering', 'Business', 'Design', 'Creative', 'Leadership']
    }, {
        category: 'Location',
        values: ['New York City', 'Los Angeles']
    }
];

module.exports = {
    searchFilters
};
