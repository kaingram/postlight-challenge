
import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

// Assets
import logo from './assets/logo.svg';

// Components
import Loader from './components/ui/loader';
import Admin from './components/Admin/Index';
import EmployeeForm from './components/Employee/Form/Index';
import Search from './components/Search/Index';

// CSS
import './App.css';

// Data Initialization
const employees = require('./data.json').results;
sessionStorage.setItem('postlightEmployees', JSON.stringify(employees));

class App extends React.Component {
  constructor() {
    super()
    this.state = { 
      isLoading: true 
    }
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        isLoading: false
      });
    }, 1500);
  }

  render () {
    return (
      <Router>
        { this.state.isLoading ?  (
          <Loader />
        ) : (
          /*********************************/
          /************* NAVBAR ************/
          /*********************************/ 
          <div className='u-height--100%'>
            <header className='site-header'>
              <div className='site-header__item u-display--flex'>
                <Link to='/'><img src={ logo } className='logo' alt='Postlight' /></Link>
              </div>
              <div className='site-header__item u-display--flex u-justify-content--center'>
                <h1 className='u-m--none u-color--white u-ls--1.75px u-white-space--nowrap'>Employee Directory</h1>
              </div>
              <div className='site-header__item u-display--flex'>
                <Link className='site-header__link u-ml--auto u-ls--1.75px' to='/admin'>Admin</Link>
              </div>
            </header>
            {/*********************************/}
            {/************* ROUTES ************/}
            {/*********************************/}
            <Route path='/' 
              exact 
              render={ () => {
                return (
                  <Search data={ employees } />
                )
              }} />
            <Route path='/admin/' 
              exact 
              component={ Admin } />
            <Route path='/admin/new' 
              render={ () => {
                return (
                  <EmployeeForm title='Create New' />
                )
              }} />
            <Route path='/admin/:id/edit' 
              render={ () => {
                return (
                  <EmployeeForm title='Edit' />
                )
              }} />
          </div>
        )}
      </Router>
    );
  }
}

export default App;
