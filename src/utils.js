
// Sort items by object property
const sortByProperty = (prop, data) => {
  let path = prop ? prop.split('.') : null;
  
  const getValueFromObjPath = (item) => {
    let valueToSortOn = null;

    if (path) {
      valueToSortOn = path.reduce((total, currentValue) => {
        return total[currentValue];
      }, item);
    }
    
    return valueToSortOn;
  };
  
  return data.sort((a, b) => {
    const valueToSortOnA = getValueFromObjPath(a);
    const valueToSortOnB = getValueFromObjPath(b);
  
    if (valueToSortOnA < valueToSortOnB) {
      return -1;
    }

    if (valueToSortOnA > valueToSortOnB ){
      return 1;
    }

    return 0;
  });
}

// Get full name of employee
const getName = (obj) => {
  let str = null;
    
  obj.first && obj.last ? str = `${obj.first} ${obj.last}` :
    obj.first ? str = obj.first :
      obj.last ? str = obj.last :
      str = 'N/A';
    
  return convertToTitleCase(str);
};
  
// TODO: Make exceptions for acronyms and/or stopwords 
const convertToTitleCase = (str) => {
  return str.replace(/\w\S*/g, (item) => {
    return item.charAt(0).toUpperCase() + item.substr(1).toLowerCase();
  });
};
 
// Copies node's value of targeted item and copies to computer clipboard
const copyToClipboard = (event, text) => {
  const currentTarget = event.currentTarget || null;
  let tooltipContent = currentTarget.getElementsByClassName('tooltip__content') || null;
  let prevTooltipValue = tooltipContent[0].innerHTML;

  tooltipContent[0].innerHTML = 'Copied!';

  // Create node to temporarily append value to
  let textArea = document.createElement("textarea");
  textArea.style.position = 'fixed';
  textArea.style.top = 0;
  textArea.style.left = 0;
  textArea.style.width = '2em';
  textArea.style.height = '2em';
  textArea.style.padding = 0;
  textArea.style.border = 'none';
  textArea.style.outline = 'none';
  textArea.style.boxShadow = 'none';
  textArea.style.background = 'transparent';

  textArea.value = text;
  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();
  document.execCommand('copy');    
  document.body.removeChild(textArea);

  // Reset tooltip to original text
  setTimeout(() => {
    tooltipContent[0].innerHTML = prevTooltipValue;
  }, 3000);
};

// Utilizes PlaceKitten API to create placeholder images at random
const generateRandomPlaceholderImage = () => {
  const randomNumber = Math.floor(Math.random() * 16) + 1;

  return `https://placekitten.com/g/400/400?image=${randomNumber}`;
}

module.exports = {
  convertToTitleCase,
  copyToClipboard,
  getName,
  generateRandomPlaceholderImage,
  sortByProperty
};
