
import React from 'react';
import { Link } from 'react-router-dom';

import * as utils from '../../utils';

class Admin extends React.Component {
    constructor() {
        super()
        this.state = {
            items: []
        };
    }

    deleteItem(item) {
        const data = sessionStorage.getItem('postlightEmployees');
        const employees = data ? JSON.parse(data) : [];
        let indexToRemove = null;
        
        for (const index in employees) {
            if (item.id === employees[index].id) {
                indexToRemove = index;
                break;
            }
        }

        if (indexToRemove) {
            employees.splice(indexToRemove, 1);
        }

        sessionStorage.setItem('postlightEmployees', JSON.stringify(employees));
        this.setState({ 
            items: utils.sortByProperty('name.last', employees) 
        });
    }
    
    componentDidMount() {
        const data = sessionStorage.getItem('postlightEmployees');
        const employees = data ? JSON.parse(data) : [];

        this.setState({ 
            items: utils.sortByProperty('name.last', employees) 
        });
    }

    render() {
        const employees = this.state.items && this.state.items.length > 0 ? (
            <ul className='u-ul--reset u-color--primary'>
                { this.state.items.map((item) => {
                    return (
                        <li className='u-bg--gray-odd u-p--xs'>
                            <div className='u-display--flex u-align-items--center'>
                                <span className='ls--1.75px'>{ utils.getName(item.name) }</span>
                                <div className='u-ml--auto'>
                                    <Link to={ `/admin/${item.id}/edit` }>
                                        <button className='btn'>Edit</button>
                                    </Link>
                                    <button className='btn u-ml--sm'
                                        onClick={ () => { this.deleteItem(item) } }>Remove</button>
                                </div>
                            </div>
                        </li>
                    );
                }) }
            </ul>
        ) : null;
    
        return (
            <div>
                <section className='u-display--flex u-align-items--center u-plr--lg u-mtb'>
                    <h1 className='u-mtb--xs'>Current Employees</h1>
                    <Link to='/admin/new' className='u-ml--auto'>
                        <button className='btn'>Create New</button>
                    </Link>
                </section>
                <div className='u-plr--lg u-mtb'>
                    { employees }
                </div>
            </div>
        );
    }
}

export default Admin;
