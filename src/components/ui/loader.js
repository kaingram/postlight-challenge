
import React from 'react';

import loader from '../../assets/loader.svg';

const Loader = () => {
    return (
        <div className='u-height--100%'>
            <div className='loader'>
                <div className='loader__image'>
                <img src={ loader } alt='Loader' />
                </div>
            </div>
        </div>
    );
}

export default Loader;
