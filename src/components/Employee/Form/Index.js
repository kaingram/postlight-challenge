
import React from 'react';
import { withRouter } from 'react-router-dom';

import * as utils from '../../../utils';

// Unique ID Generator Module
const uuidv1 = require('uuid/v1');
const generateRandomId = () => {
    return uuidv1(); // Ex.) '2c5ea4c0-4067-11e9-8bad-9b1deb4d3b7d'
};

class EmployeeForm extends React.Component {
    constructor(props) {
        super(props);
        // TODO: Convert into Employee 'Class'  
        this.state = {
            id: '',
            firstName: '',
            lastName: '',
            email: '',
            phone: '',
            cell: '',
            jobTitle: '',
            department: '',
            location: '',
            picture: ''
        };
    }

    componentDidMount() {
        // Check route parameters to determine if form should be populated or empty
        if (this.props.match.params && this.props.match.params.id) {
            const idToFind = this.props.match.params.id;
            const data = sessionStorage.getItem('postlightEmployees');
            const employees = JSON.parse(data);
            let itemToUpdate = null;
            
            for (const index in employees) {
                if (idToFind === employees[index].id) {
                    itemToUpdate = employees[index];
                    break;
                }
            }

            this.setState({
                id: itemToUpdate.id || '',
                firstName: itemToUpdate['name'].first || '',
                lastName: itemToUpdate['name'].last || '',
                email: itemToUpdate.email || '',
                phone: itemToUpdate.phone || '',
                cell: itemToUpdate.cell || '',
                jobTitle: itemToUpdate.jobTitle || '',
                department: itemToUpdate.department || '',
                location: itemToUpdate.location || '',
                picture: itemToUpdate.picture || ''
            });
        }
    }
    
    handleChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }
    
    handleSubmit(event) {
        event.preventDefault();

        const data = sessionStorage.getItem('postlightEmployees');
        const employees = JSON.parse(data);

        const result = {
            id: this.state.id || generateRandomId(),
            name: {
                first: this.state.firstName,
                last: this.state.lastName
            },
            email: this.state.email,
            phone: this.state.phone,
            cell: this.state.cell,
            jobTitle: this.state.jobTitle,
            department: this.state.department,
            location: this.state.location,
            picture: this.state.picture || utils.generateRandomPlaceholderImage()
        };

        // Check route parameters to determine if form should look for existing item or add new item
        if (this.props.match.params && this.props.match.params.id) {
            let indexToUpdate = null;
            
            for (const index in employees) {
                if (this.state.id === employees[index].id) {
                    indexToUpdate = index;
                    break;
                }
            }

            if (indexToUpdate) {
                employees.splice(indexToUpdate, 1);
            }
        }

        employees.push(result);
        sessionStorage.setItem('postlightEmployees', JSON.stringify(employees));
        // After submitting, redirect
        this.props.history.push('/admin');
    }

    render() {
        // TODO: Create reusable input/button components
        return (
            <div className='u-display--flex u-flex-direction--column u-align-items--center u-justify-content--center u-plr--lg'>
                <section className="u-text-align--center u-plr--lg u-mtb">
                    <h1 className="u-mtb--xs">{ this.props.title } Employee</h1>
                </section>
                <form onSubmit={ (e) => { this.handleSubmit(e) } }
                    className='form u-display--flex u-flex-direction--column u-color--primary u-mtb'>
                    <div className='u-display--flex u-align-items--center u-p--xs'>
                        <label>First Name:</label>
                        <input type="text"
                            name='firstName' 
                            value={ this.state.firstName } 
                            onChange={ (e) => { this.handleChange(e) } } 
                            className='form__input u-flex-grow--1' />
                    </div>
                    <div className='u-display--flex u-align-items--center u-p--xs'>
                        <label>Last Name:</label>
                        <input type="text"
                            name='lastName' 
                            value={ this.state.lastName } 
                            onChange={ (e) => { this.handleChange(e) } } 
                            className='form__input u-flex-grow--1' />
                    </div>
                    <div className='u-display--flex u-align-items--center u-p--xs'>
                        <label>Email:</label>
                        <input type="email"
                            name='email' 
                            value={ this.state.email } 
                            onChange={ (e) => { this.handleChange(e) } } 
                            className='form__input u-flex-grow--1' />
                    </div>
                    <div className='u-display--flex u-align-items--center u-p--xs'>
                        <label>Phone:</label>
                        <input type="text"
                            name='phone' 
                            value={ this.state.phone } 
                            onChange={ (e) => { this.handleChange(e) } } 
                            className='form__input u-flex-grow--1' />
                    </div>
                    <div className='u-display--flex u-align-items--center u-p--xs'>
                        <label>Cell:</label>
                        <input type="text"
                            name='cell' 
                            value={ this.state.cell } 
                            onChange={ (e) => { this.handleChange(e) } } 
                            className='form__input u-flex-grow--1' />
                    </div>
                    <div className='u-display--flex u-align-items--center u-p--xs'>
                        <label>Job Title:</label>
                        <input type="text"
                            name='jobTitle' 
                            value={ this.state.jobTitle } 
                            onChange={ (e) => { this.handleChange(e) } } 
                            className='form__input u-flex-grow--1' />
                    </div>
                    <div className='u-display--flex u-align-items--center u-p--xs'>
                        <label>Department:</label>
                        <select value={ this.state.department }
                            defaultValue=''
                            name='department' 
                            onChange={ (e) => { this.handleChange(e) } }
                            className='form__input form__input--select u-flex-grow--1'>
                            <option disabled value=''>Choose an Option</option>
                            <option value="Business">Business</option>
                            <option value="Creative">Creative</option>
                            <option value="Design">Design</option>
                            <option value="Engineering">Engineering</option>
                            <option value="Leadership">Leadership</option>
                        </select>
                    </div>
                    <div className='u-display--flex u-align-items--center u-p--xs'>
                        <label>Location:</label>
                        <select value={ this.state.location }
                            defaultValue=''
                            name='location' 
                            onChange={ (e) => { this.handleChange(e) } }
                            className='form__input form__input--select u-flex-grow--1'>
                            <option disabled value=''>Choose an Option</option>
                            <option value="los angeles">Los Angeles</option>
                            <option value="new york city">New York City</option>
                        </select>
                    </div>
                    <div className='u-ml--auto u-p--xs'>
                        <input type="submit" 
                            value="Submit" 
                            className='btn' />
                    </div>
                </form>
            </div>
        );
    }
}

export default withRouter(EmployeeForm);
