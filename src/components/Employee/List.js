
import React from 'react';

import * as utils from '../../utils';

const EmployeeList = (props) => {
  const persons = props.persons && props.persons.length > 0 ? props.persons.map((item, index) => {
    return (
      <div key={ index } className='card'>
        <img className='card__image'
          src={ utils.generateRandomPlaceholderImage() } alt={ utils.getName(item.name) } />
        <div className='card__caption u-word-break--bw'>
          <h2 className='u-m--none u-mb--xs u-ls--1.75px u-text-align--center'>{ utils.getName(item.name) }</h2>
          <h3 className='u-m--none u-mb--xs u-fs--italic u-text-align--center'>{ utils.convertToTitleCase(item.jobTitle) }</h3>
          <p className="u-display--flex u-m--none">
            <span className='icon u-mlr--sm u-hover--fade-secondary' 
              onClick={ (e) => { utils.copyToClipboard(e, item.email) } }>
                <span className='tooltip'>
                  <span className="tooltip__content">Click to Copy</span>
                  <i className="far fa-envelope"></i>
                </span>
            </span>
            <span className='icon u-mlr--sm u-hover--fade-secondary'
              onClick={ (e) => { utils.copyToClipboard(e, item.phone) } }>
                <span className='tooltip'>
                  <span className="tooltip__content">Click to Copy</span>
                  <i className="fas fa-phone-alt"></i>
                </span>
            </span>
          </p>
        </div>
      </div>
    )
  }) :
    <div className="u-color--primary u-plr--md">No results found.</div>;

    return (
      <div className='u-bg--primary-x-light u-flex-grow--1'>
        <div className='grid'>
          { persons }
        </div>
      </div>
    );
  };

export default EmployeeList;
