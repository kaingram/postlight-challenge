
import React from 'react';

import * as config from '../../config';

class SearchFilters extends React.Component {

    onFilterChange(event, category, value) {
        const currentTarget = event.currentTarget;
        const activeFilters = this.props.activeFilters;
        let activeValues = null;

        const activeFilterIndex = activeFilters.findIndex((item) => {
            return item.category === category;
        }) || null;

        if (activeFilterIndex !== -1) {
            activeValues = activeFilters[activeFilterIndex].values || null;
            const activeValueIndex = activeValues.indexOf(value) || null;
            
            if (activeValueIndex > -1) {
                activeValues.splice(activeValueIndex, 1)
            } else {
                activeValues.push(value);
            }

            activeFilters[activeFilterIndex].values = activeValues;
        } else {
            activeValues = [];
            activeValues.push(value);
            activeFilters.push({ category, values: activeValues})
        }

        // Create checkbox checkmark icon
        let icon = document.createElement('i');
        icon.className = 'fas fa-check u-color--primary';

        if (currentTarget.tagName.toLowerCase() === 'div') {     
            if (currentTarget.querySelector('i')) {
                currentTarget.childNodes[0].removeChild(currentTarget.childNodes[0].childNodes[0]);
            } else {
                currentTarget.querySelector('span').appendChild(icon);
            }
        }
        this.props.onFilterChange(activeFilters);
    }

    render() {
        return (
            <div className={ this.props.filtersAreVisible ? 
                'u-display--flex u-ptb--sm' : 
                'u-display--flex u-ptb--sm hidden' }>
                { config.searchFilters.map((filter, index) => {
                    return (
                        <div key={ index } 
                            className='u-mr--lg'>
                            <span className='u-tt--uppercase'>{ filter.category }</span>
                            { filter.values.map((value, index) => {
                                return (
                                    <div key={ index } 
                                        className='u-display--flex u-align-items--center u-ptb--xs u-color--gray'
                                        onClick={ (e) => { this.onFilterChange(e, filter.category, value) }}>
                                        <span className='input__checkbox u-mr--xs'></span>
                                        { value }
                                    </div>         
                                );
                            }) }
                        </div>
                    );
                }) }    
            </div>
        );
    }
}

export default SearchFilters;
