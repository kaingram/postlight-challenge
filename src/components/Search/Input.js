
import React from 'react';

class SearchInput extends React.Component {

    handleSearchInput(e) {
        const value = e.target.value || '';
        this.props.onSearchChange(value);
    }

    render() {
        return (
            <div className='u-plr u-ml--auto u-min-width--400px'>
                <input className='search__input u-width--100% u-color--primary' 
                    type='text'
                    placeholder='Search by name'
                    onChange={ (e) => { this.handleSearchInput(e) } } />
            </div>            
        );
    }
}

export default SearchInput;
