
import React from 'react';

import * as utils from '../../utils';

// Components
import Loader from '../ui/loader';
import EmployeeList from '../Employee/List';
import SearchFilters from './Filters';
import SearchInput from './Input';

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchQuery: '',
            filteredItems: [],
            items: [],
            isLoading: true,
            activeFilters: [],
            filtersAreVisible: false
        };
    }
   
    componentDidMount() {
        const data = sessionStorage.getItem('postlightEmployees');
        const employees = data ? JSON.parse(data) : this.props.data;

        this.setState({
            filteredItems: employees,
            items: employees
        }, () => { 
            setTimeout(() => {
                this.setState({
                    isLoading: false
                });
            }, 1500);
        });
    }

    onSearch() {
        const searchResults = this.state.items.filter((item) => {
            const query = this.state.searchQuery.toLowerCase();
            const firstName = (item.name.first || '').toLowerCase();
            const lastName = (item.name.last || '').toLowerCase();

            return (firstName.indexOf(query) > -1 || lastName.indexOf(query) > -1) ?
                item :
                null;
        }) || [];

        this.setState((prevState) => {
            return {
                filteredItems: prevState.activeFilters.length > 0 ? 
                    this.checkForActiveFilters(searchResults) : 
                        searchResults,
                isLoading: true
            }
        }, () => {
            setTimeout(() => {
                this.setState({
                    isLoading: false
                });
            }, 275);
        });
    }

    // Handles changes to search input value and reruns onSearch()
    handleSearchChange(value) {
        this.setState({
            searchQuery: value.toLowerCase()
        }, () => {
            this.onSearch();
        });
    }

    // Toggles visibility of container with filters
    toggleFilterContainer(event) {
        const currentTarget = event.currentTarget;
    
        this.setState((prevState) => {
            return {  
                filtersAreVisible: !prevState.filtersAreVisible 
            };
        }, () => {
            // Create up/down 'caret' icon
            let caretIcon = document.createElement('i');

            if (currentTarget.tagName.toLowerCase() === 'div') {     
                if (currentTarget.querySelector('i').classList.contains('fa-angle-down')) {
                    caretIcon.className = 'fas fa-angle-up';
                } else {
                    caretIcon.className = 'fas fa-angle-down';
                }
                currentTarget.querySelector('span').removeChild(currentTarget.querySelector('span').childNodes[0]);
                currentTarget.querySelector('span').appendChild(caretIcon);
            }
        });
    }

    // Accounts for check for filters after user inputs a search term (as well as before)
    checkForActiveFilters(data) {
        const activeFilters = this.state.activeFilters;
        const filteredResults = data.filter((item) => {
            let match = false;

            activeFilters.forEach((filter) => {
                const categoryToCheck = filter.category.toLowerCase();
                const valuesToCheck = filter.values;

                if (item[categoryToCheck]) {
                    if (valuesToCheck.length > 0) {
                        if (valuesToCheck.indexOf(item[categoryToCheck]) > -1) {
                            match = true;
                        }
                    }
                }
            });

            return match ? item : null;
        }) || [];

        return filteredResults.length > 0 ? filteredResults : data;
    }

    // Updates results displayed as filters are added/removed
    handleFilterChange(filters) {
        const activeFilters = filters;
        const query = this.state.searchQuery.toLowerCase();
        const filteredResults = this.state.items.filter((item) => {
            const firstName = (item.name.first || '').toLowerCase();
            const lastName = (item.name.last || '').toLowerCase();
            let match = false;

            activeFilters.forEach((filter) => {
                const categoryToCheck = filter.category.toLowerCase();
                const valuesToCheck = filter.values;

                if (item[categoryToCheck]) {
                    if (valuesToCheck.length > 0) {
                        if (valuesToCheck.indexOf(item[categoryToCheck]) > -1) {
                            match = true;
                        }
                    }
                }
            });

            if (match) {
                if (query !== '') {
                    if (firstName.indexOf(query) > -1 || lastName.indexOf(query) > -1) {
                        return item;
                    }
                }
                return item; // TODO: Default
            }
        }) || [];

        this.setState((prevState) => {
            return {
                activeFilters, 
                filteredItems: filteredResults.length > 0 ? filteredResults : prevState.items,
                isLoading: true
            }
        }, () => {
            setTimeout(() => {
                this.setState({
                  isLoading: false
                });
            }, 275);
        });
    }
    
    render() {
        return (
            <div className='u-display--flex u-flex-direction--column u-height--100%'>
                <div className='u-display--flex u-flex-direction--column u-bg--secondary-x-light'>
                    <div className='u-display--flex u-align-items--center u-plr'>
                        <div className="u-ptb--sm u-width--50%">
                            <div onClick={ (e) => { this.toggleFilterContainer(e) } }
                                className="u-display--flex u-align-items--center u-cursor--pointer u-white-space--nowrap">
                                Filter by 
                                <span className="u-ml--xs">
                                    <i className="fas fa-angle-down"></i>
                                </span>
                            </div>
                        </div>
                        <SearchInput onSearchChange={ (value) => { this.handleSearchChange(value) } } />
                    </div>
                    <div className="u-plr">
                        <SearchFilters filtersAreVisible={ this.state.filtersAreVisible }
                            activeFilters={ this.state.activeFilters }
                            onFilterChange={ (e) => { this.handleFilterChange(e) }}  />
                    </div>        
                </div>
                { this.state.isLoading ? 
                    ( <Loader /> ) : 
                    ( <EmployeeList persons={ utils.sortByProperty('name.last', this.state.filteredItems) } /> )
                }
            </div>
        );
    }
}

export default Search;
